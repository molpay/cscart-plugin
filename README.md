MOLPay CS-Cart Plugin
==================

MOLPay Plugin for CS-Cart Shopping Cart develop by MOLPay technical team.


Notes
-----

MOLPay Sdn. Bhd. is not responsible for any problems that might arise from the use of this module. 
Use at your own risk. Please backup any critical data before proceeding. For any query or 
assistance, please email support@molpay.com


Installations for CS-Cart version 4.x.x and above
------------------------------------------------------

- Download this plugin, Extract/Unzip the files. 

- Upload or copy those file and folder into your CS-Cart root folder

- (Skip this if your CS-Cart is not hosted in UNIX environment). 
Please ensure the file permission is correct. It's recommended to CHMOD to 644

- Go to any browser (Mozilla, Google Chrome, Internet Explorer, etc), 
copy and paste the URL shown below into address bar on your browser. 
(The purpose of this action is to register MOLPay Payment plugin into the database).
    
     `http://www.your-cscart.com/setup_molpay.php`

    ***Replace `www.your-cscart.com` with URL address of your shopping cart.

- Login as CS-Cart Store Admin, go to `Administration` > `Payment Methods`.

- Click `Add Payment` to create new payment method.

- Refer to the datail below, kindly insert details into the respective fields: 
    1. Name: Molpay
    2. Processor: MOLPay Malaysia Online Payment
    3. Template:(optional)
    4. Payment Category: Internet Payments
    5. Another fields is optional

- Click on `[Create]` button on the bottom to save all changes.

- Click on `edit` button at �MOLPay Malaysia Online Payment� Payment methods.

- Click `Configure`.

- Fill in the details accordingly: 
    1. MOLPay Merchant ID : Your MOLPay Merchant ID 
    2. MOLPay Verify Key  : Your MOLPay Verify key. Check your merchant profile to have this key.

- Save all the changes. You�re Done!


Contribution
------------

You can contribute to this plugin by sending the pull request to this repository.


Issues
------------

Submit issue to this repository or email to our support@molpay.com


Support
-------

Merchant Technical Support / Customer Care : support@molpay.com <br>
Sales/Reseller Enquiry : sales@molpay.com <br>
Marketing Campaign : marketing@molpay.com <br>
Channel/Partner Enquiry : channel@molpay.com <br>
Media Contact : media@molpay.com <br>
R&D and Tech-related Suggestion : technical@molpay.com <br>
Abuse Reporting : abuse@molpay.com